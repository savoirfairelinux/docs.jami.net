# docs.jami.net

## DEPRECATED

This repository and its static pages are deprecated in favour of
https://git.jami.net/savoirfairelinux/jami-docs which uses Sphinx and
is currently deployed to https://docs.jami.net.  The home page there
links to the pages linked to in this repository's index.html file.

## Old README

This is a simple static page to group principal links used to document the [Jami](https://jami.net) project.

![](img/fullpage.png)
